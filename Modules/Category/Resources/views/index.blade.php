@extends('dashboard::layouts.master')

@section('content')
<section id="main-content">
    <section class="wrapper">

        <!-- overstart -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="col-md-6">
                            <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}} </li>
                        </div>
                        <div class="col-md-6">
                            <li class="text-right"><i class="fa fa-plus"></i><a href="{{route('category.create')}}">Add</a></li>
                        </div>

                    </div>
                </ol>
            </div>
        </div>
        <!-- end of overstart -->

        <!-- main content -->
        <!-- start of category table -->
        <div class="row">
            <div class="col-xs-12">
                <!--left body: categorytable -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <!--start heading of the table  -->
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Status</th>
                                <th>Created At</th>

                                
                 
                                <th colspan="2" style="text-align: center;">Setting</th>
                            </tr>
                        </thead>
                        <!-- end of table heading -->
                        <!-- table body start -->
                        <tbody>
                          @foreach($data['category'] as $category)
                          <tr>
                              <td>{{$category->category}}</td>
                              
                              
                                    @if($category->status == 0)
                                        <td>
                                            @if($category->status == 0)
                                                <a href="{{route('category.status',$category->id)}}" class="btn btn-xs  btn-danger">Inactive</a>
                                            @else
                                                <a href="" class="btn btn-xs btn-info">Active</a>
                                            @endif
                                        </td>
                                    @else
                                        <td>
                                            @if($category->status == 1)
                                            <a href="{{route('category.status',$category->id)}}" class="btn btn-xs  btn-info">Active</a>

                                            @else
                                            <a href="" class="btn btn-xs btn-danger">Inactive</a>
                                            @endif
                                        </td>
                                    @endif
                            <td>
                                @if(!$category->created_at == NULL)
                                {{$category->created_at->format('M-d-Y')}}
                                @else
                                <?php echo 'Null'; ?>
                                @endif
                            </td>
                            <td><a href="{{Route('category.edit',$category->id)}}" class="btn btn-xs btn-info">Edit</a>
                            </td>
                            <td><a id="delete" href="{{Route('category.delete',$category->id)}}" class="btn btn-xs btn-danger">Delete</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                    <!-- end of table body -->
                </table>
            </div>
            <!-- bottom pagination -->
                <div class="col-lg-4">
                    <center>
                            {!! $data['category']->render() !!}
                    </center>
                </div>
            <!-- end of bottom pagination -->
            <!-- leftbody : categorytable end -->
        </div>
    </div>

</section>
</section>


@endsection

@extends('dashboard::layouts.master')

@section('title')


{{$_panel}} :: Create


@endsection
@section('content')
<section id="main-content">
  <section class="wrapper">
    {{-- overview --}}
    <div class="row">
      <div class="col-lg-12">
        <!-- Title Bar -->
        <ol class="breadcrumb">
          <div class="row">
            <div class="col-md-6">
              <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}}</li>
            </div>
            <div class="col-md-6">
              <li class="text-right"><i class="fa fa-eye"></i><a href="{{Route('category')}}">View</a></li>
            </div>

          </div>
        </ol>


        <!-- End of Title Bar -->
      </div>


    </div>
    {{-- overviewend --}}

    {{-- form started --}}
    <form method="post" action="{{Route('category.store')}}" enctype="multipart/form-data" onsubmit="return checkForm(this);">
      @csrf
      <div class="row">
            <div class="form-wrapper well">
              <div class="form-group">
                     <!-- name -->
                <div class="row">
                  <div class="col-sm-2">
                    <label class="control-label">Category:</label>
                  </div>

                  <div class="col-sm-8">
                    <input type="text" class="form-control"  placeholder="Enter Company's name" name="category" value="{{old('category')}}" required>
                    @if($errors->has('category'))
                    <span class="text-danger">
                      *{{$errors->first('category')}}
                    </span>
                    @endif
                  </div>

                </div>
                <!-- end of name -->
                <br>
              </div> 
                <button type="submit" name="myButton" class="btn btn-primary"><b>Save</b></button>
            </div>
     
     </div>
  </form>
  {{-- form end --}}


  </section>
</section>
@endsection

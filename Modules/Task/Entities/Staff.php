<?php

namespace Modules\Task\Entities;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $fillable = ['name','description','status'];
    public function task()
    {
        return $this->belongsToMany(Task::class);
    }
}

<?php

namespace Modules\Task\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Modules\Task\Entities\Staff;

class StaffController extends BaseController
{
    protected $view_path = 'task::staff.';
    protected $base_route = 'staff';
    protected $databaseimage = '';
    protected $folder = '';
    protected $panel = 'Staff';
    protected $folder_path;

    public function __construct(Staff $model){
        $this->model = $model;
        // $this->folderpath = public_path('images'. DIRECTORY_SEPARATOR).$this->folder;
    }

    public function index()
    {
        $data['staff'] =Staff::paginate(10);
        return view(parent::commondata($this->view_path.'index'),compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view(parent::commondata($this->view_path.'create'));
    }


    public function store(Request $request)
    {
        $data['staff'] = Staff::create($request->all());
        Session::flash('success','Task Created successfully');
        return redirect()->route($this->base_route);
    }

    public function edit($id)
    {
        $data['staff'] =Staff::find($id);
        $this->rowExist($data['staff']);
        return view(parent::commondata($this->view_path.'edit'),compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data['staff'] = Staff::find($id);
        $data['staff']->update($request->all());
        Session::flash('success','Task update successfully');
        return redirect()->route($this->base_route);
    }

    public function status($id){
        $this->statuschange($id);
        return redirect()->route($this->base_route);
    }

    public function destroy($id)
    {
        $data['staff'] = Staff::find($id);
        $this->rowExist($data['staff']);
        $data['staff']->delete();
        Session::flash('warning','Staff deleted Successfully');
        return redirect()->Route($this->base_route);
    }



}

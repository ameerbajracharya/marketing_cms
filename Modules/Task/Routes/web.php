<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//task
Route::group(['middleware' => ['auth','web'],'prefix' => 'dashboard/task'], function() {
    Route::get('/', 			['as' => 'task', 			'uses' => 'TaskController@index']);
    Route::get('/create', 			['as' => 'task.create', 			'uses' => 'TaskController@create']);
    Route::post('/store', 			['as' => 'task.store', 			'uses' => 'TaskController@store']);
    Route::get('/edit/{id}', 			['as' => 'task.edit', 			'uses' => 'TaskController@edit']);
    Route::post('/update/{id}', 			['as' => 'task.update', 			'uses' => 'TaskController@update']);
    Route::get('/status/{id}', 			['as' => 'task.status', 			'uses' => 'TaskController@status']);
    Route::get('/delete/{id}', 			['as' => 'task.delete', 			'uses' => 'TaskController@destroy']);
});

//staff
Route::group(['middleware' => ['auth','web'],'prefix' => 'dashboard/staff'], function() {
    Route::get('/', 			        ['as' => 'staff', 			'uses' => 'StaffController@index']);
    Route::get('/create', 			     ['as' => 'staff.create', 			'uses' => 'StaffController@create']);
    Route::post('/store', 			    ['as' => 'staff.store', 			'uses' => 'StaffController@store']);
    Route::get('/status/{id}', 			['as' => 'staff.status', 			'uses' => 'StaffController@status']);
    Route::get('/delete/{id}', 			['as' => 'staff.delete', 			'uses' => 'StaffController@destroy']);
    Route::get('/edit/{id}', 			['as' => 'staff.edit', 			'uses' => 'StaffController@edit']);
    Route::post('/update/{id}', 		['as' => 'staff.update', 			'uses' => 'StaffController@update']);

});

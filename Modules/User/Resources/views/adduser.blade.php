@extends('dashboard::layouts.master')

@section('content')
<section id="main-content">
    <section class="wrapper">

        <!-- overstart -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="col-md-6">
                            <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}}</li>
                        </div>
                        <div class="col-md-6">
                            <li class="text-right"><i class="fa fa-eye"></i><a href="{{route('user')}}">View</a></li>
                        </div>

                    </div>
                </ol>
            </div>
        </div>
        <!-- end of overstart -->

        <!-- main content -->
        <!-- start of user table -->
        <div class="row">
            <div class="col-md-12">
             <div class="card-body">
                <form method="POST" action="{{ route('user.store') }}" onsubmit="return checkForm(this);">
                    @csrf

                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-2 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="text" class="col-md-2 col-form-label text-md-right">{{ __('Address') }}</label>

                        <div class="col-md-6">
                            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address">

                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                     <label for="user_type" class="col-md-2 col-form-label text-md-right">User Type</label>

                     <div class="col-md-6">
                         <select id="user_type" type="string" class="form-control{{ $errors->has('user_type') ? ' is-invalid' : '' }}" name="user_type" required>
                            <option>user</option>
                            <option>superadmin</option>
                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="password" class="col-md-2 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm" class="col-md-2 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" name="myButton" class="btn btn-primary">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</section>
</section>


@endsection

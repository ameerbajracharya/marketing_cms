@extends('dashboard::layouts.master')

@section('content')
<section id="main-content">
    <section class="wrapper">

        <!-- overstart -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="col-md-6">
                            <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}} </li>
                        </div>
                        <div class="col-md-6">
                            <li class="text-right"><i class="fa fa-plus"></i><a href="{{route('user.create')}}">Add</a></li>
                        </div>

                    </div>
                </ol>
            </div>
        </div>
        <!-- end of overstart -->

        <!-- main content -->
        <!-- start of user table -->
        <div class="row">
            <div class="col-xs-12">
                <!--left body: usertable -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <!--start heading of the table  -->
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email Address</th>
                                <th>Address</th>
                                <th>User Type</th>
                                <th>Created</th>
                                <th colspan="2" style="text-align: center;">Setting</th>
                            </tr>
                        </thead>
                        <!-- end of table heading -->
                        <!-- table body start -->
                        <tbody>
                          @foreach($data['user'] as $user_info)
                          <tr>
                              <td>{{$user_info->name}}</td>
                              <td>{{$user_info->email}}</td>
                              <td>{{$user_info->address}}</td>
                              <td>
                                @if($user_info->user_type == 'user')
                                <a href="{{route('user.user_type',$user_info->id)}}" class="btn btn-xs btn-danger">User</a>
                                @else
                                <a href="{{route('user.user_type',$user_info->id)}}" class="btn btn-xs btn-info">SuperAdmin</a>
                                @endif
                            </td>
                            <td>
                                @if(!$user_info->created_at == NULL)
                                {{$user_info->created_at->format('M-d-Y')}}
                                @else
                                <?php echo 'Null'; ?>
                                @endif
                            </td>
                            <td><a href="{{Route('user.edit',$user_info->id)}}" class="btn btn-xs btn-info">Edit</a>
                            </td>
                            <td><a id="delete" href="{{Route('user.delete',$user_info->id)}}" class="btn btn-xs btn-danger">Delete</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                    <!-- end of table body -->
                </table>
            </div>
            <!-- bottom pagination -->
                <div class="col-lg-4">
                    <center>
                            {!! $data['user']->render() !!}
                    </center>
                </div>
            <!-- end of bottom pagination -->
            <!-- leftbody : usertable end -->
        </div>
    </div>

</section>
</section>


@endsection

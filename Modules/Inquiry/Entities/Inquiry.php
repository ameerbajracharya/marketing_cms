<?php

namespace Modules\Inquiry\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\FollowUp\Entites\FollowUp;
use Modules\Category\Entites\Category;
use App\User;

class Inquiry extends Model
{
    protected $fillable = ['category','company','user_id','address','email','contactperson','phoneno','mobileno','website','software','remarks','facebook','image'];
    protected $dates = ['created_at'];
    public function followup()
    {
    	return $this->hasMany(FollowUp::class);
    }
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public  function categories()
    {
        return $this->belongsTo(Category::class);
    }
}

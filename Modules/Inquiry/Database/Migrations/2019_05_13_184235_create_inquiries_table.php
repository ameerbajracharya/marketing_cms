<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->string('category');
            $table->string('company');
            $table->string('address');
            $table->string('email')->nullable();
            $table->boolean('status')->default(0);
            $table->string('contactperson')->nullable();
            $table->integer('phoneno');
            $table->biginteger('mobileno')->nullable();
            $table->string('website')->nullable();
            $table->string('software')->nullable();
            $table->string('remarks')->nullable();
            $table->string('facebook')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiries');
    }
}

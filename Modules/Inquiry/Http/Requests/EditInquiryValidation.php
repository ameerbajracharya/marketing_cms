<?php

namespace Modules\Inquiry\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditInquiryValidation extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'email' => 'unique',
          'company'=>'required',
          'address'=>'required',
          'phoneno'=>'required|min:1',
      ];
  }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

<?php

namespace Modules\Inquiry\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddInquiryValidation extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company'=>'required',
            'address'=>'required',
            'phoneno'=>'required|max:9|min:1',
            'mobileno'=>"max:10",
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

<?php

namespace Modules\Inquiry\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\BaseController;
use Modules\Inquiry\Entities\Inquiry;
use Modules\Category\Entities\Category;
use App\User;
use Validator;
use Session;
use Modules\Inquiry\Http\Requests\AddInquiryValidation;
use Modules\Inquiry\Http\Requests\EditInquiryValidation;
use Intervention\Image\ImageManager;
use Image;
use Auth;
use DB;

class InquiryController extends BaseController
{

 protected $view_path = 'inquiry::';
 protected $base_route = 'inquiry.view';
 protected $databaseimage = 'image';
 protected $folder = 'inquiry';
 protected $panel = 'Company';
 protected $folder_path;




 public function __construct(Inquiry $model){
  $this->model = $model;
  $this->folderpath = ('public'. DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR).$this->folder;
}

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
      return view('inquiry::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
      $data['category'] = Category::all();
      return view(parent::commondata($this->view_path.'createinquiry'),compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(AddInquiryValidation $request)
    {
      $data['inquiry'] = Inquiry::create([
        'user_id' => Auth::user()->id,
        'category' => $request->category,
        'company' => $request->company,
        'address' => $request->address,
        'email' => $request->email,
        'contactperson' => $request->contactperson,
        'phoneno' => $request->phoneno,
        'mobileno' => $request->mobileno,
      ]); 
      Session::flash('success','Inquiry Stored Successfully');
      return redirect()->Route('inquiry.view');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function view()
    {
      if(Inquiry::count() > 0)
      {
        if(Auth::user()->name == 'admin')
        {
          $data['inquiry'] = Inquiry::paginate(8);
          return view(parent::commondata($this->view_path.'listinquiry'),compact('data'));
        }
        else
        {
         $data['inquiry'] = Inquiry::where('user_id', Auth::user()->id )->paginate(8);
         return view(parent::commondata($this->view_path.'listinquiry'),compact('data'));
       }
     }
     else
     {
      Session::flash('warning','Inquiry is Empty.');
      return redirect()->route('inquiry.create');
    }
  }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
      $data['inquiry'] = Inquiry::find($id);
      return view(parent::commondata($this->view_path.'showinquiry'),compact('data'));
    }

    public function edit($id)
    {
      $data['inquiry'] = Inquiry::find($id);
      $data['category'] = Category::all();
      return view(parent::commondata($this->view_path.'editinquiry'),compact('data'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(EditInquiryValidation $request, $id)
    {
      $data = Inquiry::find($id);
      $this->storeimage($request, $id);
      $data->update($request->all());
      Session::flash('success','Inquiry Updated Successfully.');
      return redirect()->Route($this->base_route);    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
      $data = Inquiry::find($id);
      //$this->rowExist($data);
      $this->destroydata($id);
      Session::flash('success','Inquiry Deleted Successfully.');
      return redirect()->Route($this->base_route);    }
    }

<!DOCTYPE html>
<html>
<head>
	@include('dashboard::include.header')
</head>
<body>
    <section id="container">
    @include('dashboard::include.navbar')
    
    @yield('content')
    @include('dashboard::include.footer')
    </section>
    @yield('js')
   
   <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
</body>
</html>

@extends('dashboard::layouts.master')
@section('title')


    {{$_panel}}


@endsection
@section('content')

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i>Home</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a></li>
              <!-- <li><i class="fa fa-laptop"></i>Dashboard</li> -->
            </ol>
          </div>
        </div>
            @can('isAdmin')
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{Route('user')}}">
              <div class="info-box brown-bg">
                <i class="fa fa-user" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Users </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>
          <!--/.col-->
          @endcan


           <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{Route('inquiry.view')}}">
              <div class="info-box red-bg">
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Comp List</h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>
          <!--/.col-->


           <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{Route('followup.show')}}">
              <div class="info-box blue-bg">
                <i class="fa fa-bullhorn" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Inquiries </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>
          <!--/.col-->
        </div>

      </section>
    </section>
    <!-- container section start -->
@stop

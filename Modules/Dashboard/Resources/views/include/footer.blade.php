<script src="{{asset('public/frontend/js/jquery.js')}}"></script>
<!-- javascripts -->
<script src="{{asset('public/backend/js/jquery.js')}}"></script>
<script src="{{asset('public/backend/js/jquery-ui-1.10.4.min.js')}}"></script>
<script src="{{asset('public/backend/js/jquery-1.8.3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/backend/js/jquery-ui-1.9.2.custom.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('public/backend/js/bootstrap.min.js')}}"></script>
<!-- nice scroll -->
<script src="{{asset('public/backend/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{asset('public/backend/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<!-- charts scripts -->

<!--script for this page only-->
<!-- custom select -->
<!--custome script for all page-->
<script src="{{asset('public/backend/js/scripts.js')}}"></script>
<script src="{{asset('public/backend/js/jquery.autosize.min.js')}}"></script>
<script src="{{asset('public/js/toastr.min.js')}}"></script>



<script>
  toastr.options = {
    "debug": false,
    "positionClass": "toast-top-right",
    "onclick": null,
    "fadeIn": 200,
    "fadeOut": 2000,
    "timeOut": 5000,
    "extendedTimeOut": 1000
  }

  @if(Session::has('success'))
  toastr.success("{{Session::get('success')}}");
  @endif

  @if(Session::has('warning'))
  toastr.error("{{Session::get('warning')}}");
  @endif


</script>

<script>
  $('#my-selector').bind('click', function() {
    $(this).unbind('click');
    return confirm("Submit Informations?");
  });
</script>

<script>
  $('#delete').on('click', function() {
    return confirm("Are You Sure You Want To Delete?");
  });
</script>

<script type="text/javascript">

    function checkForm(form)
    {
        //
        // validate form fields
        //

        form.myButton.disabled = true;
        return true;
    }

</script>

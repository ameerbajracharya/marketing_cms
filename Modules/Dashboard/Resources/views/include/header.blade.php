  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <title>@yield('title')</title>
  <!-- Bootstrap CSS -->
  <link href="{{asset('public/backend/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="{{asset('public/backend/css/bootstrap-theme.css')}}" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="{{asset('public/backend/css/elegant-icons-style.css')}}" rel="stylesheet" />
  <link href="{{asset('public/backend/css/font-awesome.min.css')}}" rel="stylesheet" />
  <!-- owl carousel -->
  <link href="{{asset('public/backend/css/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet">
  <!-- Custom styles -->
  <link href="{{asset('public/backend/css/style.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('public/backend/stylesheets/main.css')}}">
  <link href="{{asset('public/backend/css/style-responsive.css')}}" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('public/css/toastr.min.css')}}" />
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>


<header class="header dark-bg">
    <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
    </div>
    <!--logo start-->
    <a href="{{$dashboard}}" class="logo">KITE<span class="lite"> Marketing Team</span></a>
    <!--logo end-->

<div class="top-nav notification-row nav-collapse">
    <!-- notificatoin dropdown start-->
    <ul class="nav pull-right top-menu">
        <p class="nav-link dropdown-toggle" style="color:#fed189" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
            <span>
                <h4><i class="fa fa-user"></i> {{Auth::user()->name}}/{{Auth::user()->user_type}}</h4>
            </span>
        </p>
    </ul>
</div>
</header>
<!--header end-->

<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
          <li class="active">
            <form method="get" action="{{route('search')}}">
              <input class="form-control" placeholder="Search" type="text" name="search" style="width: 99.9%;">
          </form>
      </li>
      <li class="active">
        <a class="" href="{{$dashboard}}">
            <i class="icon_house_alt"></i>
            <span>Home</span>
        </a>
    </li>
    @can('isAdmin')
    <li class="nav-item">
        <a class="nav-link " href="{{route('user.create')}}" role="button">
            <i class="fa fa-user"></i>
            <span>User</span>
        </a>
    </li>
    @endcan

    <li class="nav-item">
        <a class="nav-link" href="{{route('inquiry.create')}}" role="button">
             <i class="fa fa-plus-square-o"></i>
            <span>Company Create</span>
        </a>
    </li>

    <li class="nav-item ">
        <a class="nav-link" href="{{route('followup.show')}}" role="button">
            <i class="fa fa-bullhorn"></i>
            <span>Company Inquiry</span>
        </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{route('client')}}" role="button">
         <i class="fa fa-users"></i>
            <span>Client</span>
        </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{route('category')}}" role="button">
            <i class="fa fa-folder"></i>

            <span>Category</span>
        </a>
    </li>
    <li class="nav-item"><a style="color:red" href="{{route('logout')}}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit()" ;>
    <i class="icon_key_alt"></i>
    Log Out
</a>
<form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
    {{@csrf_field()}}
</form>
</li>
</ul>
<!-- sidebar menu end-->
</div>
</aside>
<!--sidebar end-->

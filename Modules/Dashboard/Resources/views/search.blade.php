@extends('dashboard::layouts.master')

@section('title')


Search Result


@endsection

@section('content')
<section id="main-content">
	<section class="wrapper">

		<!-- overstart -->
		<div class="row">
			<div class="col-lg-12">
				<ol class="breadcrumb">
					<div class="row">
						<div class="col-md-6">
							<li><i class="fa fa-home"></i><a href="{{$dashboard}}">Home</a> | {{$_panel}}</li>
						</div>
						<div class="col-md-6">
							<li class="text-right"><a href="{{Route('inquiry.create')}}"><i class="fa fa-plus"></i>Add Company</a></li>
						</div>

					</div>
				</ol>
			</div>
		</div>
		<!-- end of overstart -->
		<h1 class="stunning-header-title">Search Result For: {{ $search}} </h1>
		<!-- main content -->
		<!-- start of user table -->
		<div class="row">
			<div class="col-lg-12">
				<!--left body: usertable -->
				@if($data['followup']->count() > 0)
				<div class="table-responsive">
					<table class="table">
						<!--start heading of the table  -->
						<thead>
							<tr>
								<th>S.N.</th>
								<th>Company</th>
								<th>Date</th>
								<th>Follow Up</th>
								<th>Next Date</th>
								<th>Created Date</th>
								<th colspan="2" style="text-align: center;">Action</th>

							</tr>
						</thead>
						<!-- end of table heading -->
						<!-- table body start -->
						<tbody>
							@foreach ($data['followup'] as $followup)
							<tr>
								<th scope="row">{{$loop->iteration}}</th>
								<td>{{$followup->icompany}}</td>
								<td>{{$followup->date}}</td>
								<td>{{$followup->followup}}</td>
								<td>{{$followup->nextdate}}</td>
								<td>{{$followup->created_at->format('Y-M-d')}}</td>
								<td style="text-align: center;">
									<a class="btn btn-xs btn-info" href="{{Route('followup.edit',$followup->id)}}" style=" margin-right:15px;">
									Edit</a>
									<a id="delete" class="btn btn-xs btn-danger" href="{{Route('followup.delete',$followup->id)}}">Delete</a>
								</td>
							</tr>

							@endforeach

						</tbody>
						<!-- end of table body -->
					</table>
				</div>
				@elseif($data['inquiry']->count() > 0)
				<div class="table-responsive">
					<table class="table">
						<!--start heading of the table  -->
						<thead>
							<tr>
								<th>S.N.</th>
								<th>Company</th>
								<th>Address</th>
								<th>Email</th>
								<th>Phone Number</th>
								<th>Contact Person</th>
								<th>Mobile Number</th>
								<th>Created</th>
								<th colspan="3" style="text-align: center;">Action</th>

							</tr>
						</thead>
						<!-- end of table heading -->
						<!-- table body start -->
						<tbody>
							@foreach ($data['inquiry'] as $inquiry)
							<tr>
								<th scope="row">{{$loop->iteration}}</th>
								<td>{{$inquiry->company}}</td>
								<td>{{$inquiry->address}}</td>
								<td>{{$inquiry->email}}</td>
								<td>{{$inquiry->phoneno}}</td>
								<td>{{$inquiry->contactperson}}</td>
								<td>{{$inquiry->mobileno}}</td>
								<td>{{$inquiry->created_at->format('Y-M-d')}}</td>
								<td>
									<a type="button" class="btn btn-xs btn-primary" href="{{Route('inquiry.view',$inquiry->id)}}">View</a>
								</td>
								<td>
									<a type="button" class="btn btn-xs btn-success" href="{{Route('inquiry.edit',$inquiry->id)}}">Edit</a>
								</td>
								<td>
									<a id="delete" class="btn btn-xs btn-danger" href="{{Route('inquiry.delete',$inquiry->id)}}">Delete</a>
								</td>
							</tr>

							@endforeach

						</tbody>
						<!-- end of table body -->
					</table>
				</div>
				@else
				<h2 class="text-center">No Results Found On Follow Up Table.</h2>
				@endif
				<!-- bottom pagination -->
				<center>
					<div class="col-lg-12">

					</div>
				</center>
				<!-- end of bottom pagination -->
				<!-- leftbody : usertable end -->
			</div>
		</div>

		<!-- bottom pagination -->

		<!-- end of bottom pagination -->

	</section>
</section>


@endsection

<?php

namespace Modules\Dashboard\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Inquiry\Entities\Inquiry;
use Modules\Followup\Entities\Followup;
use Session;

class DashboardController extends BaseController
{
    protected $view_path = 'dashboard::';
    protected $base_route = 'dashboard';
    protected $panel = 'Dashboard';

    public function index()
    {
         return view(parent::commondata($this->view_path.'dashboard_index'));
    }

    public function search()
    {
    	$data['inquiry'] = Inquiry::where('created_at','like', '%'. request('search') . '%')->get();	
    	$data['followup'] = Followup::where('date','like', '%'. request('search') . '%')->get();
    	return view(parent::commondata($this->view_path.'search'),compact('data'))
    	->with('company','Search Result:'. request('search'))
    	->with('search', request('search'));
    }
}

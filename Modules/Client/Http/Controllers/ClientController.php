<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\BaseController;
use Modules\Client\Entities\Client;
use Modules\Category\Entities\Category;

use Modules\Client\Http\Requests\AddClientValidation;

use Session;

use DB;


class ClientController extends BaseController
{

    protected $view_path = 'client::';
    protected $base_route = 'client';
    protected $databaseimage = '';
    protected $folder = '';
    protected $panel = 'client';
    protected $folder_path;

  public function __construct(Client $model){
        $this->model = $model;
        // $this->folderpath = public_path('images'. DIRECTORY_SEPARATOR).$this->folder;
    }
    
    public function view()
    {
    $data['client'] = Client::paginate(10);
      return view(parent::commondata($this->view_path.'index'), compact('data'));
    }


    public function create()
    {
        $data['category'] = Category::all();
        return view(parent::commondata($this->view_path.'add'), compact('data'));
    }

    public function store(AddClientValidation $request)
    {
        $data['client']= Client::create($request->all());
        Session::flash('success','Client Stored Successfully');
        return redirect()->route('client');
    }


    public function edit($id)
    {
        $data['category'] = Category::find($id);
        $data['client'] = Client::find($id);
        $data['category'] = Category::all();
        return view(parent::commondata($this->view_path.'edit'),compact('data'));
    }
    public function show($id)
    {
        $data['client'] = client::find($id);
        return view(parent::commondata($this->view_path.'show'),compact('data'));
    }

    public function update(AddClientValidation $request, $id)
    {
        $data = client::find($id);
        $data->update($request->all());
        Session::flash('success','Client Updated Successfully.');
        return redirect()->Route($this->base_route);    }

    public function delete($id)
    {
      $data = client::find($id);
      $data->delete();
      Session::flash('success','ClientDeleted Successfully.');
      return redirect()->Route($this->base_route); 
         }

     public function status($id){
        $this->statuschange($id);
        return redirect()->route($this->base_route);
    }
  }

<?php

namespace Modules\Client\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddClientValidation extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'address'=>'required',
            'contact'=>'required|numeric',
            'mobileno'=>'required|numeric',
            'email'=>'required',
            'contractdate' => 'required',
            'expdate' => 'required',
            'contactperson'=>'required',
             'about'=>'required',

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
